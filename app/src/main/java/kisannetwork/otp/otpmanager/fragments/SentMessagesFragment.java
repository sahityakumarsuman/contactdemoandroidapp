package kisannetwork.otp.otpmanager.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kisannetwork.otp.otpmanager.R;
import kisannetwork.otp.otpmanager.adapters.ContactListAdapter;
import kisannetwork.otp.otpmanager.adapters.SentMessageListAdapter;
import kisannetwork.otp.otpmanager.models.AllContactsModels;
import kisannetwork.otp.otpmanager.models.AllSentMessgesModels;
import kisannetwork.otp.otpmanager.utils.ApiConstants;
import kisannetwork.otp.otpmanager.utils.CommonMethods;
import timber.log.Timber;

/**
 * Created by sahitya on 30/9/18.
 */

public class SentMessagesFragment extends Fragment {


  @BindView(R.id.progress_view_layout)
  RelativeLayout progress_view_layout;

  @BindView(R.id.contact_layout_RV)
  RecyclerView contact_layout_RV;

  @BindView(R.id.retry_button)
  TextView retry_button;
  @BindView(R.id.internet_connection_not_RL)
  RelativeLayout internet_connection_not_RL;

  @BindView(R.id.refresh_button)
  FloatingActionButton refresh_button;


  private Unbinder unbinder = null;

  private String API_URL = null;

  private SentMessageListAdapter _sentMessageListAdapter = null;


  public SentMessagesFragment() {

  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View root_view = inflater.inflate(R.layout.sent_messages_fragment_layout, container, false);
    unbinder = ButterKnife.bind(this, root_view);
    get_all_contacts_from_server();
    return root_view;
  }


  @OnClick(R.id.retry_button)
  void retry() {
    if (CommonMethods.isConnected(getActivity()) && internet_connection_not_RL.getVisibility() == View.VISIBLE) {
      internet_connection_not_RL.setVisibility(View.GONE);
      get_all_contacts_from_server();
    } else {
      progress_view_layout.setVisibility(View.GONE);
      contact_layout_RV.setVisibility(View.GONE);
      internet_connection_not_RL.setVisibility(View.VISIBLE);
    }
  }


  @OnClick(R.id.refresh_button)
  void refresh_button() {
    if (progress_view_layout.getVisibility() == View.GONE) {
      contact_layout_RV.setVisibility(View.GONE);
      progress_view_layout.setVisibility(View.VISIBLE);
      get_all_contacts_from_server();
    }
  }


  private void get_all_contacts_from_server() {


    if (progress_view_layout.getVisibility() == View.GONE) {
      contact_layout_RV.setVisibility(View.GONE);
      progress_view_layout.setVisibility(View.VISIBLE);
    }

    API_URL = ApiConstants.BASE_URL + ApiConstants.GET_ALL_SENT_MESSAGES;

    Timber.d("Api Calling " + API_URL);

    AndroidNetworking.get(API_URL)
            .addHeaders(ApiConstants.CONTENT_TYPE, ApiConstants.APPLICATION_JSON)
            .setPriority(Priority.IMMEDIATE)
            .build()
            .getAsObject(AllSentMessgesModels.class, new ParsedRequestListener<AllSentMessgesModels>() {
              @Override
              public void onResponse(AllSentMessgesModels response) {
                Timber.d("Response from server" + response.getData().getMessage());

                if (response != null && response.getData() != null) {

                  if (progress_view_layout != null && contact_layout_RV != null) {
                    progress_view_layout.setVisibility(View.GONE);
                    contact_layout_RV.setVisibility(View.VISIBLE);
                  }

                  _sentMessageListAdapter = new SentMessageListAdapter(getActivity(), response.getData().getUser_list());
                  DividerItemDecoration divider = new
                          DividerItemDecoration(getContext(),
                          DividerItemDecoration.VERTICAL);
                  divider.setDrawable(ContextCompat.getDrawable(getActivity(),
                          R.drawable.line_divider));
                  contact_layout_RV.addItemDecoration(divider);
                  LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                  contact_layout_RV.setLayoutManager(layoutManager);
                  contact_layout_RV.setItemAnimator(new DefaultItemAnimator());
                  contact_layout_RV.setAdapter(_sentMessageListAdapter);

                } else {

                }
              }

              @Override
              public void onError(ANError anError) {
                Timber.d("Error ::" + anError.getErrorDetail());
              }
            });
  }


  @Override
  public void onDestroyView() {

    if (unbinder != null)
      unbinder.unbind();
    super.onDestroyView();
  }
}
