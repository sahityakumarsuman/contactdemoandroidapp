package kisannetwork.otp.otpmanager.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sahitya on 30/9/18.
 */

public class AllSentMessgesModels {
  /**
   * data : {"message":"All sent messages","users_list":[{"_id":"5bb0c0a312f5a01ea91f8f69","sent_message_to":"917506224343","sent_message_from":"8950301164","message":"Your OTP is 2981hello there I am batman","message_timestamp":"1538310307735","__v":0}]}
   */
  @SerializedName("data")
  private ResponseData data;

  public ResponseData getData() {
    return data;
  }

  public void setData(ResponseData data) {
    this.data = data;
  }

  public static class ResponseData {
    /**
     * message : All sent messages
     * users_list : [{"_id":"5bb0c0a312f5a01ea91f8f69","sent_message_to":"917506224343","sent_message_from":"8950301164","message":"Your OTP is 2981hello there I am batman","message_timestamp":"1538310307735","__v":0}]
     */

    @SerializedName("message")
    private String message;
    @SerializedName("users_list")
    private ArrayList<UsersListBean> user_list;

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public ArrayList<UsersListBean> getUser_list() {
      return user_list;
    }

    public void setUser_list(ArrayList<UsersListBean> user_list) {
      this.user_list = user_list;
    }

    public static class UsersListBean {
      /**
       * _id : 5bb0c0a312f5a01ea91f8f69
       * sent_message_to : 917506224343
       * sent_message_from : 8950301164
       * message : Your OTP is 2981hello there I am batman
       * message_timestamp : 1538310307735
       * __v : 0
       */

      @SerializedName("_id")
      private String id;
      @SerializedName("sent_message_to")
      private String sent_message_to;
      @SerializedName("sent_message_from")
      private String sent_message_from;
      @SerializedName("message")
      private String message;
      @SerializedName("message_timestamp")
      private String message_timestamp;
      @SerializedName("__v")
      private int v;
      @SerializedName("sent_message_pic")
      private String sent_message_pic;

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }

      public String getSent_message_to() {
        return sent_message_to;
      }

      public void setSent_message_to(String sent_message_to) {
        this.sent_message_to = sent_message_to;
      }

      public String getSent_message_from() {
        return sent_message_from;
      }

      public void setSent_message_from(String sent_message_from) {
        this.sent_message_from = sent_message_from;
      }

      public String getMessage() {
        return message;
      }

      public void setMessage(String message) {
        this.message = message;
      }

      public String getMessage_timestamp() {
        return message_timestamp;
      }

      public void setMessage_timestamp(String message_timestamp) {
        this.message_timestamp = message_timestamp;
      }

      public int getV() {
        return v;
      }

      public void setV(int v) {
        this.v = v;
      }

      public String getSent_message_pic() {
        return sent_message_pic;
      }

      public void setSent_message_pic(String sent_message_pic) {
        this.sent_message_pic = sent_message_pic;
      }
    }
  }
}
