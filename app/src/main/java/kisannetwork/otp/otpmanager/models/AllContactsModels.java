package kisannetwork.otp.otpmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sahitya on 30/9/18.
 */

public class AllContactsModels implements Parcelable{
  /**
   * data : {"message":"All users","created_data":[{"_id":"5bb0ab04d3dd006e06ef6534","name":"sahitya","profile_url":"batman","contact_number":"nothing","email":"bhosda","abv_count":43674634,"__v":0},{"_id":"5bb0ab2e555b196e79f759f4","name":"sahityaafdkjad","profile_url":"batmanfadsfkjka","contact_number":"afdsfkasdfnothing","email":"bhosdafdajdsfjjk","abv_count":43674634,"__v":0},{"_id":"5bb0bbb647133d17136cd405","name":"sahityaafdkjad","profile_url":"batmanfadsfkjka","contact_number":"afdsfkas8429823dfnothing","email":"bhosdafdajdsfjjk","abv_count":43674634,"__v":0},{"_id":"5bb0bcbd38d0b219374699b6","name":"how are ayou","profile_url":"hello there","contact_number":"i am batman","email":"sahityakumar@gmail.com","abv_count":333,"__v":0},{"_id":"5bb0c11c12f5a01ea91f8f6a","name":"how are ayou","profile_url":"hello there","contact_number":"i am batman who are you","email":"sahityakumar@gmail.com","abv_count":333,"__v":0}]}
   */

  @SerializedName("data")
  private ContactResponse data;

  protected AllContactsModels(Parcel in) {
  }

  public static final Creator<AllContactsModels> CREATOR = new Creator<AllContactsModels>() {
    @Override
    public AllContactsModels createFromParcel(Parcel in) {
      return new AllContactsModels(in);
    }

    @Override
    public AllContactsModels[] newArray(int size) {
      return new AllContactsModels[size];
    }
  };

  public ContactResponse getData() {
    return data;
  }

  public void setData(ContactResponse data) {
    this.data = data;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
  }

  public static class ContactResponse {
    /**
     * message : All users
     * created_data : [{"_id":"5bb0ab04d3dd006e06ef6534","name":"sahitya","profile_url":"batman","contact_number":"nothing","email":"bhosda","abv_count":43674634,"__v":0},{"_id":"5bb0ab2e555b196e79f759f4","name":"sahityaafdkjad","profile_url":"batmanfadsfkjka","contact_number":"afdsfkasdfnothing","email":"bhosdafdajdsfjjk","abv_count":43674634,"__v":0},{"_id":"5bb0bbb647133d17136cd405","name":"sahityaafdkjad","profile_url":"batmanfadsfkjka","contact_number":"afdsfkas8429823dfnothing","email":"bhosdafdajdsfjjk","abv_count":43674634,"__v":0},{"_id":"5bb0bcbd38d0b219374699b6","name":"how are ayou","profile_url":"hello there","contact_number":"i am batman","email":"sahityakumar@gmail.com","abv_count":333,"__v":0},{"_id":"5bb0c11c12f5a01ea91f8f6a","name":"how are ayou","profile_url":"hello there","contact_number":"i am batman who are you","email":"sahityakumar@gmail.com","abv_count":333,"__v":0}]
     */

    @SerializedName("message")
    private String message;

    @SerializedName("created_data")
    private ArrayList<CreatedDataList> contact_list;

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public ArrayList<CreatedDataList> getContact_list() {
      return contact_list;
    }

    public void setContact_list(ArrayList<CreatedDataList> contact_list) {
      this.contact_list = contact_list;
    }

    public static class CreatedDataList implements Parcelable {
      /**
       * _id : 5bb0ab04d3dd006e06ef6534
       * name : sahitya
       * profile_url : batman
       * contact_number : nothing
       * email : bhosda
       * abv_count : 43674634
       * __v : 0
       */

      @SerializedName("_id")
      private String id;
      @SerializedName("name")
      private String name;
      @SerializedName("profile_url")
      private String profile_url;
      @SerializedName("contact_number")
      private String contact_number;
      @SerializedName("email")
      private String email;
      @SerializedName("abv_count")
      private int abv_count;
      @SerializedName("__v")
      private int v;

      protected CreatedDataList(Parcel in) {
        id = in.readString();
        name = in.readString();
        profile_url = in.readString();
        contact_number = in.readString();
        email = in.readString();
        abv_count = in.readInt();
        v = in.readInt();
      }

      public static final Creator<CreatedDataList> CREATOR = new Creator<CreatedDataList>() {
        @Override
        public CreatedDataList createFromParcel(Parcel in) {
          return new CreatedDataList(in);
        }

        @Override
        public CreatedDataList[] newArray(int size) {
          return new CreatedDataList[size];
        }
      };

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getProfile_url() {
        return profile_url;
      }

      public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
      }

      public String getContact_number() {
        return contact_number;
      }

      public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
      }

      public String getEmail() {
        return email;
      }

      public void setEmail(String email) {
        this.email = email;
      }

      public int getAbv_count() {
        return abv_count;
      }

      public void setAbv_count(int abv_count) {
        this.abv_count = abv_count;
      }

      public int getV() {
        return v;
      }

      public void setV(int v) {
        this.v = v;
      }

      @Override
      public int describeContents() {
        return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(profile_url);
        dest.writeString(contact_number);
        dest.writeString(email);
        dest.writeInt(abv_count);
        dest.writeInt(v);
      }
    }
  }
}
