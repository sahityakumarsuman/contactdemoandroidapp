package kisannetwork.otp.otpmanager.app;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

import timber.log.Timber;

/**
 * Created by sahitya on 30/9/18.
 */

public class OptManagerApplication extends Application {


  private static OptManagerApplication application;


  public static synchronized OptManagerApplication getInstance() {
    return application;
  }

  public static OptManagerApplication getApplicationInstance() {
    if (application != null)
      return application;
    return null;
  }


  @Override
  public void onCreate() {
    super.onCreate();

    application = this;
    AndroidNetworking.initialize(getApplicationContext());


    Timber.plant(new Timber.DebugTree() {
      @Override
      protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + " : " + element.getLineNumber();
      }
    });
  }
}
