package kisannetwork.otp.otpmanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kisannetwork.otp.otpmanager.activity.ProfileDetails;
import kisannetwork.otp.otpmanager.widgets.CircularImageView;
import kisannetwork.otp.otpmanager.R;
import kisannetwork.otp.otpmanager.app.OptManagerApplication;
import kisannetwork.otp.otpmanager.models.AllContactsModels;
import kisannetwork.otp.otpmanager.utils.ObjectUtil;
import timber.log.Timber;

/**
 * Created by sahitya on 30/9/18.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyCustomViewHolder> {

  ArrayList<AllContactsModels.ContactResponse.CreatedDataList> _mAllContactList = null;
  private Context _mContext = null;
  private LayoutInflater _mLayoutInflater = null;

  public ContactListAdapter(Context context, ArrayList<AllContactsModels.ContactResponse.CreatedDataList> allContactList) {
    this._mContext = context;
    this._mAllContactList = allContactList;
    this._mLayoutInflater = LayoutInflater.from(_mContext);

    Timber.d("Lenght of the lis t" + allContactList.size());
  }


  @NonNull
  @Override
  public ContactListAdapter.MyCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = _mLayoutInflater.inflate(R.layout.contact_item_layout, parent, false);
    ButterKnife.bind(this, itemView);
    return new ContactListAdapter.MyCustomViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull ContactListAdapter.MyCustomViewHolder holder, int position) {

    final AllContactsModels.ContactResponse.CreatedDataList single_user = _mAllContactList.get(position);

    holder.user_name_tv.setText(single_user.getName());
    holder.phone_number.setText(single_user.getContact_number());
    holder.viewCountData.setText(single_user.getAbv_count() + "");

    if (single_user.getProfile_url() != null && !single_user.getProfile_url().isEmpty()) {
      RequestOptions requestOptions = new RequestOptions();
      requestOptions.fitCenter();
      requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
      Log.d("tracking", single_user.getProfile_url());
      Log.d("tracking", "" + single_user.getProfile_url());
      Glide.with(OptManagerApplication.getApplicationInstance()
              .getApplicationContext())
              .setDefaultRequestOptions(requestOptions)
              .load(single_user.getProfile_url())
              .into(holder.user_image);

    }


    holder.parent_rl.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Timber.d("Here we are bitch");
        Intent next_intent = new Intent(_mContext, ProfileDetails.class);
        next_intent.putExtra("user_details", single_user);
        _mContext.startActivity(next_intent);

      }
    });


  }

  @Override
  public int getItemCount() {
    if (!ObjectUtil.isNull(_mAllContactList) && _mAllContactList.size() > 0)
      return _mAllContactList.size();
    return 0;
  }

  public class MyCustomViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_image)
    ImageView user_image;
    @BindView(R.id.user_name_tv)
    TextView user_name_tv;
    @BindView(R.id.phone_number)
    TextView phone_number;
    @BindView(R.id.viewCountData)
    TextView viewCountData;
    @BindView(R.id.parent_rl)
    RelativeLayout parent_rl;

    public MyCustomViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
