package kisannetwork.otp.otpmanager.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import kisannetwork.otp.otpmanager.R;
import kisannetwork.otp.otpmanager.app.OptManagerApplication;
import kisannetwork.otp.otpmanager.models.AllSentMessgesModels;
import kisannetwork.otp.otpmanager.utils.ObjectUtil;
import kisannetwork.otp.otpmanager.widgets.CircularImageView;
import timber.log.Timber;


/**
 * Created by sahitya on 30/9/18.
 */

public class SentMessageListAdapter extends RecyclerView.Adapter<SentMessageListAdapter.MyCustomViewHolder> {


  private ArrayList<AllSentMessgesModels.ResponseData.UsersListBean> _mAllSentMessages = null;
  private Context _mContext = null;
  private LayoutInflater _mLayoutInflater = null;

  public SentMessageListAdapter(Context context, ArrayList<AllSentMessgesModels.ResponseData.UsersListBean> allSentMessages) {
    this._mContext = context;
    this._mAllSentMessages = allSentMessages;
    this._mLayoutInflater = LayoutInflater.from(_mContext);
    Timber.d("Sent user list" + allSentMessages.size());
    Collections.reverse(_mAllSentMessages);
  }


  @NonNull
  @Override
  public SentMessageListAdapter.MyCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = _mLayoutInflater.inflate(R.layout.sent_message_item_layout, parent, false);
    ButterKnife.bind(this, itemView);
    return new SentMessageListAdapter.MyCustomViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull SentMessageListAdapter.MyCustomViewHolder holder, int position) {
    AllSentMessgesModels.ResponseData.UsersListBean single_user = _mAllSentMessages.get(position);
    holder.message_sent_to_Tv.setText(single_user.getSent_message_to() + "");
    holder.message_descript_tv.setText(single_user.getMessage());
    holder.sent_time_tv.setReferenceTime(Long.parseLong(single_user.getMessage_timestamp()));

    if (single_user.getSent_message_pic() != null && !single_user.getSent_message_pic().isEmpty()) {
      RequestOptions requestOptions = new RequestOptions();
      requestOptions.fitCenter();
      requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
      Log.d("tracking", single_user.getSent_message_pic());
      Glide.with(OptManagerApplication.getApplicationInstance()
              .getApplicationContext())
              .setDefaultRequestOptions(requestOptions)
              .load(single_user.getSent_message_pic())
              .into(holder.message_profile_pic);

    }

  }

  @Override
  public int getItemCount() {
    if (!ObjectUtil.isNull(_mAllSentMessages) && _mAllSentMessages.size() > 0)
      return _mAllSentMessages.size();
    return 0;
  }

  public class MyCustomViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.message_sent_to_Tv)
    TextView message_sent_to_Tv;
    @BindView(R.id.message_descript_tv)
    TextView message_descript_tv;
    @BindView(R.id.sent_time_tv)
    RelativeTimeTextView sent_time_tv;
    @BindView(R.id.message_profile_pic)
    CircularImageView message_profile_pic;

    public MyCustomViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
