package kisannetwork.otp.otpmanager.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kisannetwork.otp.otpmanager.R;
import kisannetwork.otp.otpmanager.adapters.ContactListAdapter;
import kisannetwork.otp.otpmanager.app.OptManagerApplication;
import kisannetwork.otp.otpmanager.models.AllContactsModels;
import kisannetwork.otp.otpmanager.utils.ApiConstants;
import kisannetwork.otp.otpmanager.utils.CommonMethods;
import kisannetwork.otp.otpmanager.widgets.CircularImageView;
import timber.log.Timber;

/**
 * Created by sahitya on 1/10/18.
 */

public class ProfileDetails extends AppCompatActivity {


  @BindView(R.id.fab)
  FloatingActionButton fab;

  @BindView(R.id.user_full_name)
  TextView user_full_name;

  @BindView(R.id.user_contact_number)
  TextView user_contact_number;

  @BindView(R.id.user_email_address)
  TextView user_email_address;

  @BindView(R.id.profile_image)
  CircularImageView profile_image;


  private ProgressDialog progressDialog = null;

  private AllContactsModels.ContactResponse.CreatedDataList single_user = null;
  private String API_URL = null;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.user_profile_activity_layout);
    ButterKnife.bind(this);

    if (getIntent() != null)
      single_user = getIntent().getParcelableExtra("user_details");

    Timber.d("Extra details " + single_user.getContact_number());

    user_full_name.setText(single_user.getName());
    user_contact_number.setText(single_user.getContact_number());
    user_email_address.setText(single_user.getEmail());

    if (single_user.getProfile_url() != null && !single_user.getProfile_url().isEmpty()) {
      RequestOptions requestOptions = new RequestOptions();
      requestOptions.fitCenter();
      requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
      Log.d("tracking", single_user.getProfile_url());
      Log.d("tracking", "" + single_user.getProfile_url());
      Glide.with(OptManagerApplication.getApplicationInstance()
              .getApplicationContext())
              .setDefaultRequestOptions(requestOptions)
              .load(single_user.getProfile_url())
              .into(profile_image);

    }
  }

  @OnClick(R.id.fab)
  void showDiag() {

    final View dialogView = View.inflate(this, R.layout.send_dialog_box, null);

    final Dialog dialog = new Dialog(this, R.style.MyAlertDialogStyle);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(dialogView);

    final AppCompatEditText send_message_edittext = dialog.findViewById(R.id.send_message_edittext);

    AppCompatTextView cancel_button_tv = dialog.findViewById(R.id.cancel_button_tv);
    AppCompatTextView send_button_tv = dialog.findViewById(R.id.send_button_tv);


    cancel_button_tv.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
      @Override
      public void onClick(View v) {
        revealShow(dialogView, false, dialog);
      }
    });


    send_button_tv.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String message = send_message_edittext.getText().toString();
        if (!message.isEmpty()) {
          if (CommonMethods.isConnected(ProfileDetails.this)) {
            sed_message(message, dialog, dialogView);
          } else {
            CommonMethods.showToast("Please connect to Internet", ProfileDetails.this);
          }
        } else {
          CommonMethods.showToast("Enter some message", ProfileDetails.this);
        }
      }
    });


    dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
      @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
      @Override
      public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_BACK) {
          revealShow(dialogView, false, dialog);
          return true;
        }
        return false;
      }
    });


    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    dialog.show();
  }

  private void sed_message(String message, final Dialog dialog, final View dialogView) {

    show_dialog();

    API_URL = ApiConstants.BASE_URL + ApiConstants.SEND_MESSAGES;

    Timber.d("Api Calling " + API_URL);
    AndroidNetworking.post(API_URL)
            .addHeaders(ApiConstants.CONTENT_TYPE, ApiConstants.APPLICATION_JSON)
            .addBodyParameter(ApiConstants.BODY_MESSAGE_SENT_TO, "9971792703")
            .addBodyParameter(ApiConstants.BODY_MESSAGE_SENT_FROM, single_user.getContact_number())
            .addBodyParameter(ApiConstants.BODY_MESSAGE, message)
            .setPriority(Priority.IMMEDIATE)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
              @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
              @Override
              public void onResponse(JSONObject response) {
                Timber.d("");

                dismiss_dialog();

                try {
                  String message = response.getJSONObject("data").has("message") ?
                          response.getJSONObject("data").getString("message") : "nothing";

                  if (message.contentEquals(ApiConstants.MESSAGE_SENT_SUCCESS)) {

                    CommonMethods.showToast("Message sent successful", ProfileDetails.this);
                    revealShow(dialogView, false, dialog);
                  } else {
                    CommonMethods.showToast("Something went wrong, Try again.", ProfileDetails.this);
                  }
                } catch (JSONException e) {
                  e.printStackTrace();
                }

              }

              @Override
              public void onError(ANError anError) {
                dismiss_dialog();
                Timber.d("Error :: " + anError.getErrorDetail());
                CommonMethods.showToast("Connection Error", ProfileDetails.this);
              }
            });
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  private void revealShow(View dialogView, boolean b, final Dialog dialog) {

    final View view = dialogView.findViewById(R.id.dialog);

    int w = view.getWidth();
    int h = view.getHeight();

    int endRadius = (int) Math.hypot(w, h);

    int cx = (int) (fab.getX() + (fab.getWidth() / 2));
    int cy = (int) (fab.getY()) + fab.getHeight() + 56;


    if (b) {
      Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, endRadius);

      view.setVisibility(View.VISIBLE);
      revealAnimator.setDuration(700);
      revealAnimator.start();

    } else {

      Animator anim =
              ViewAnimationUtils.createCircularReveal(view, cx, cy, endRadius, 0);

      anim.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          super.onAnimationEnd(animation);
          dialog.dismiss();
          view.setVisibility(View.INVISIBLE);

        }
      });
      anim.setDuration(700);
      anim.start();
    }
  }


  private void show_dialog() {
    if (progressDialog == null)
      progressDialog = new ProgressDialog(this, R.style.MyTheme);
    progressDialog.setCancelable(false);
    progressDialog.show();
  }

  private void dismiss_dialog() {
    if (progressDialog != null && progressDialog.isShowing())
      progressDialog.dismiss();

  }

}
