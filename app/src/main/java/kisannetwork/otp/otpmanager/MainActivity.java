package kisannetwork.otp.otpmanager;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kisannetwork.otp.otpmanager.fragments.ContactListFragment;
import kisannetwork.otp.otpmanager.fragments.SentMessagesFragment;

public class MainActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

  private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

  @BindView(R.id.toolbar)
  Toolbar toolbar;

  @BindView(R.id.tabs)
  TabLayout tabLayout;

  @BindView(R.id.viewpager)
  ViewPager viewPager;

  @BindView(R.id.materialup_appbar)
  AppBarLayout appbarLayout;

  @BindView(R.id.materialup_profile_image)
  ImageView mProfileImage;

  private int[] tabIcons = {
          R.mipmap.message_ic,
          R.mipmap.sent_message_ic,
  };
  private boolean mIsAvatarShown = true;


  private int mMaxScrollSize;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);
    appbarLayout.addOnOffsetChangedListener(this);
    mMaxScrollSize = appbarLayout.getTotalScrollRange();
    setupViewPager(viewPager);

    tabLayout.setupWithViewPager(viewPager);
    setupTabIcons();
  }


  @Override
  public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
    if (mMaxScrollSize == 0)
      mMaxScrollSize = appBarLayout.getTotalScrollRange();

    int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

    if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
      mIsAvatarShown = false;

      mProfileImage.animate()
              .scaleY(0).scaleX(0)
              .setDuration(200)
              .start();
    }

    if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
      mIsAvatarShown = true;

      mProfileImage.animate()
              .scaleY(1).scaleX(1)
              .start();
    }
  }


  private void setupTabIcons() {
    tabLayout.getTabAt(0).setIcon(tabIcons[0]);
    tabLayout.getTabAt(1).setIcon(tabIcons[1]);
  }

  private void setupViewPager(ViewPager viewPager) {
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
    adapter.addFrag(new ContactListFragment(), "Contact");
    adapter.addFrag(new SentMessagesFragment(), "Messages");
    viewPager.setAdapter(adapter);
  }

  class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
      super(manager);
    }

    @Override
    public Fragment getItem(int position) {
      return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
      return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return mFragmentTitleList.get(position);
    }
  }
}
