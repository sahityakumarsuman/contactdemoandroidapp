package kisannetwork.otp.otpmanager.utils;

/**
 * Created by sahitya on 30/9/18.
 */

public class ApiConstants {




  public static final String BASE_URL = "http://18.219.129.84:8079";
  public static final String GET_ALL_USERS = "/user/all-users";
  public static final String GET_ALL_SENT_MESSAGES = "/user/all-sent-messages";
  public static final String SEND_MESSAGES = "/user/send-message";
  public static final String CREATE_PROFILE = "/user/insert-user";

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String APPLICATION_JSON = "application/json";

  public static final String BODY_MESSAGE_SENT_TO = "contact_number_to";
  public static final String BODY_MESSAGE = "message";
  public static final String BODY_MESSAGE_SENT_FROM = "sent_message_from";
  public static final String MESSAGE_SENT_SUCCESS = "User entry successful";


}
