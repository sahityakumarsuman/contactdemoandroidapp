package kisannetwork.otp.otpmanager;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sahitya on 1/10/18.
 */

public class SplashActivity extends AppCompatActivity {


  @BindView(R.id.background_rl)
  RelativeLayout background_rl;

  private AnimationDrawable animationDrawable;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.splash_layout);
    ButterKnife.bind(this);

    animationDrawable = (AnimationDrawable) background_rl.getBackground();
    animationDrawable.setEnterFadeDuration(5000);
    animationDrawable.setExitFadeDuration(2000);
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
      }
    }, 6000);


  }


  @Override
  protected void onResume() {
    super.onResume();
    if (animationDrawable != null && !animationDrawable.isRunning())
      animationDrawable.start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (animationDrawable != null && animationDrawable.isRunning())
      animationDrawable.stop();
  }

}
